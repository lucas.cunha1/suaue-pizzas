import React, { Fragment, useState } from 'react';
import ResponsiveAppBar from './components/ResponsiveAppBar';

import {
  Switch,
  Router, Route, Redirect
} from "react-router-dom";
import Footer from './components/Footer';
import Home from './views/portal/Home';
import Sobre from './views/portal/Sobre';
import Produtos from './views/portal/Produtos';
import Servicos from './views/portal/Servicos';
import Contato from './views/portal/Contato';
import LayoutAdmin from './components/Layout/admin';
import { isAuthenticated } from './config/auth';
import Category from './views/admin/categories'
import Products from './views/admin/products'
import TelaLogin from './views/auth/login';
import history from './config/history';





function App() {
  const [isAdmin, setAdmin] = useState(false)
  const AdminRoute = ({ ...rest }) => {
    setAdmin(true)
    if (!isAuthenticated()) {
        return <Redirect to='/login' />
    }
    return <Route {...rest} />
  }
  const UserRoute = ({ ...rest }) => {
    setAdmin(false)
    return <Route {...rest} />
  }
  const LoginRoute = ({...rest}) => {
    setAdmin(true)
    return <Route {...rest} />
  }

  return (
    <Router history={history}>
      <Fragment>
          <div id='content-wrap'>
          {!isAdmin ? <ResponsiveAppBar  title={"Suaue Pizzas"} menuItems={
            [
          { title: 'Home', link: '/home' },
          // { title: 'Sobre', link: '/sobre' }, 
          { title: 'Produtos', link: '/produtos' },
          // { title: 'Serviços', link: '/servicos' }, 
          // { title: 'Contato', link: '/contato' }
          ]}>
          </ResponsiveAppBar> : ''}
            <main>
              <Switch>
              <LoginRoute exact path='/login' component={TelaLogin}></LoginRoute>

              <UserRoute path="/admin">
                <LayoutAdmin>
                    <AdminRoute exact path="/admin" component={() => <p>Home</p>} />
                    <AdminRoute exact path="/admin/categorias" component={Category} />
                    <AdminRoute exact path="/admin/produtos" component={Products} />
                </LayoutAdmin>
            </UserRoute> 
              <UserRoute exact path='/' component={Home}></UserRoute>
                <UserRoute  exact path='/home' component={Home}></UserRoute>
                <UserRoute exact path='/sobre' component={Sobre}></UserRoute>
                <UserRoute exact path='/produtos' component={Produtos}></UserRoute>
                <UserRoute exact path='/servicos' component={Servicos}></UserRoute>
                <UserRoute exact path='/contato' component={Contato}></UserRoute>
              </Switch>
            </main>
          </div>
            {!isAdmin ? <Footer></Footer> : ''}
        </Fragment>
    </Router>
  );
}

export default App;

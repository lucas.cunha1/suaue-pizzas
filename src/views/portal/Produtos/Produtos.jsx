import React, { useEffect, useState } from 'react'
import { Card, Tab, Tabs } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components'
import { fetchProducts } from '../../../store/slices/getProducts';
import { fetchCategory } from '../../../store/slices/getCategories';
import CircularProgress from '@material-ui/core/CircularProgress';

const Produtos = () => {
  const dispatch = useDispatch()
  const [products, setProducts] = useState([])
  const [categories, setCategories] = useState([])
  const slicersState = useSelector((state) => state.slicers)

  useEffect(() => {
    (async()=>{
      let data = await dispatch(fetchProducts())
      setProducts(data.payload)
    })()
    return () => {
    };
  }, [])

  useEffect(() => {
    (async ()=>{
      let data = await dispatch(fetchCategory())
      setCategories(data.payload)
    })()
    return () => {
    };
  }, [])
  const mountProducts = (cat) => {

    const prods = products.filter(item => item.category._id === cat._id)
    console.log(prods)
    return (
      <div>
        {prods.length === 0
          ? <div>Sem produtos</div>
          : (
            <div class='grid-pizza'>
            {prods.map((prd, i) => (
              <CardProducts key={i} >
              <Card.Img src={prd.photo} />
              <Card.Body>
              <Card.Title>{prd.title}</Card.Title>
              <Card.Text>
                           {prd.description}
              </Card.Text>
              </Card.Body>
            </CardProducts>
            ))}
            </div>
          )
        }
      </div >
    )
  }

  return (
    <Product>
      <ContainerTitle>
        <div className='container'>

          <Title>{'Produtos'}</Title>
          <Sub>{'Conheça nossa lista de Produtos'}</Sub>
        </div>

      </ContainerTitle>
      <div className='container'>

        {slicersState.getCategories.loading || slicersState.getProducts.loading
        ? <CircularProgressBlack/> : 
        <TabBox defaultActiveKey={1} className="produtos">
          {categories.map((cat, i) => (
            <Tab eventKey={i} key={i} title={cat.name}>
              {mountProducts(cat)}
            </Tab>
          ))}
        </TabBox>}
      </div>

    </Product>
  )
}

export default Produtos

const Product = styled.div`
    display:block;
    background: #fff;

    .produtos{
          margin - top: 20px;
    }
    .tab-content{

        }
    .tab-pane{
          display: flex;
        div{
          display: grid;
            gap: 0.6rem;
            /* grid-template-columns: repeat(auto-fit, 250px); */
            width:100%;
        }
    }
    .grid-pizza{
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      display: grid;
      grid-template-columns: repeat(auto-fit, 300px);
    }
`

const TabBox = styled(Tabs)`
    margin-top: 1rem;
    background: #E8E8E8;
`

const ContainerTitle = styled.div`
 background: #fac564;
    padding: 1.7rem;
    font-family: 'Poppins', sans-serif;

`
const Title = styled.div`
    color: #fff;
    font-size: 30px;
    font-weight: 600;
`
const Sub = styled.div`
   color: #eee;
   font-size: 20px;
   font-family: 'Josefin Sans', sans-serif;
`
const CardProducts = styled.section`
   display: inline-block;
   width: 300px;
   
`
const CircularProgressBlack = styled(CircularProgress)`
  color: black
`
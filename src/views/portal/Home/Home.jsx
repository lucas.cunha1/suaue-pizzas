import React from 'react'
import styled from 'styled-components'
import Carousel from 'react-bootstrap/Carousel'
import { Row, Col, Button } from 'react-bootstrap'
import BgBanner from '../../../assets/images/bg_1.jpg'
import Img2 from '../../../assets/images/bg_2.png'
import frangoCatupiry from '../../../assets/images/frango-catupiry.png';

const Home = () => {
  return (
    <Banner>
            <div className="bg container">
                <BannerItem>
                    <Carousel controls={false}>
                        <Carousel.Item>
                            <Row className="mt-5 py-3 justify-content-center align-items-center">
                                <Col md={3} sm={3}>
                                    <img src={frangoCatupiry} className="img-fluid" alt="" />
                                </Col>
                                <Col md={3} sm={3} className="mb-4">
                                    <div className="tag">Promoção</div>
                                    <h2 className="mb-4 title">Frango com Catupiry Vegana</h2>
                                    <div className="mb-4 desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. At labore accusamus ea repellat quisquam.</div>
                                </Col>
                            </Row>
                        </Carousel.Item>
                        <Carousel.Item>
                            <Row className="mt-5 py-3 justify-content-center align-items-center">
                                <Col md={3} sm={3}>
                                    <img src={Img2} className="img-fluid" alt="" />
                                </Col>
                                <Col md={3} sm={3} className="mb-4">
                                    <div className="tag">Promoção</div>
                                    <h2 className="mb-4 title">Pizza </h2>
                                    <div className="mb-4 desc">Lorem ipsum dolor sit amet consectetur adipisicing elit. At labore accusamus ea repellat quisquam.</div>
                                    <Button variant="danger">Comprar</Button>
                                </Col>
                            </Row>
                        </Carousel.Item>
                    </Carousel>
                </BannerItem>
            </div>
        </Banner>
  )
}

export default Home

const Banner = styled.div`
    display: ${props => props.hidden === true ? 'none' : 'block'};
    width: 100%;
    background-image: url(${BgBanner});
    background-size: 60% 100%;
    overflow: hidden;

    .bg{
        background: #0003;
    }
`

const BannerItem = styled.div`
    color: #fff
`
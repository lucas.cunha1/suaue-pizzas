import createAsyncSlice from './createAsyncSlice';
import { listarCategorias } from '../../services/categories';

const getCategories = createAsyncSlice({
    name: 'getCategories',
    service: listarCategorias
});

export const fetchCategory = getCategories.asyncAction;

export default getCategories.reducer
import createAsyncSlice from './createAsyncSlice';
import { listarProdutos } from '../../services/products';

const getProducts = createAsyncSlice({
    name: 'getProducts',
    service: listarProdutos
});

export const fetchProducts = getProducts.asyncAction;

export default getProducts.reducer
import { applyMiddleware, combineReducers, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import multi from 'redux-multi'

import categoryReducer from './categories/category.reducer'
import productReducer from './products/product.reducer'
import authReducer from './auth/auth.reducer'
import getProducts from './slices/getProducts';
import getCategories from './slices/getCategories';

const slicers = combineReducers({
    getProducts,
    getCategories
})
// modularizações dos reduces
const reducers = combineReducers({
    auth: authReducer,
    category: categoryReducer,
    product: productReducer,
    slicers: slicers
})

// middlewares de confifurações do projeto
const middleware = [thunk, multi]

// compose que junta os middlewares e ferramentas de debug
const compose = composeWithDevTools(
    applyMiddleware(...middleware)
)

// criação da store
const store = createStore(reducers, compose)

export default store
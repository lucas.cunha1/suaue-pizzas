import Swal from "sweetalert2";

const message = (type, message) => {
    Swal.fire({
        position: 'top-end',
        icon: type,
        title: message,
        showConfirmButton: false,
        timer: 2500
    })
}

export default message
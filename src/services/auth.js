import http from '../config/http'

const authentication = (data) => http.post('/auth/login', data)

export {
    authentication
}
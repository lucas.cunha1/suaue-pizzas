import http from '../config/http'

const listarProdutos = () => http.get('/product')

export {
    listarProdutos
}
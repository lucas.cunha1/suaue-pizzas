import http from '../config/http'

const listarCategorias = () => http.get('/category')

export {
    listarCategorias
}
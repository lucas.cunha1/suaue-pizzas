import { Typography } from '@material-ui/core'
import styled from 'styled-components'
import React from 'react'

const FooterCore = styled.div`
display: flex;
flex-direction: column;
color: white;
width: 100%;

article{
  padding: 1rem 1rem;
  flex-direction:column;
  flex: 1 1 0;
  display: flex;
}

article h3{
    font-weight: semibold;
    color: var(--secondaryColor);
    border-bottom: 1px solid var(--secondaryColor);
  }
  @media(min-width: 768px){
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-evenly;
    article{
      padding: 0 1rem;
    }
  }
`;

const FooterSection = styled.section`
padding: 1rem 0;
  p{
    color: white;
  }
  #copyright{
    text-align: center; 
  }
`

const FooterBox = styled.footer`
position: absolute;
  bottom: 0;
  width: 100%;
  background-color: var(--primaryColor);
  .container{
    background-color: var(--primaryColor);
  }
  @media(max-width: 767px){
    position: static;
  }
`;


export const Footer = () => {
  return (
    <FooterBox id='footer'>
        <div className='container'>
        <FooterSection>
          <FooterCore>
          <article>
          <Typography variant="h6" component="h3">
              Sobre nós
          </Typography>
          <p >Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
          </article>
          <article>
          <Typography variant="h6" component="h3">
              Nossos destaques
          </Typography>
          <p>Lorem ipsum lar dor amet, yes</p>
          </article>
          <article>
          <Typography variant="h6" component="h3">
              Onde estamos
          </Typography>
          <ul>
            <li>Rua boa morte 0, Recanto Celeste</li>
            <li>+55(85)98989-9898</li>
            <li>suauepizzas@mail.com</li>
          </ul>
          </article>
          </FooterCore>
        
        <div></div>
        <p id='copyright'>Todos os direitos reservados</p>
        </FooterSection>
        </div>
    </FooterBox>
  )
}


import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  AppBar, Toolbar, Typography, List, ListItem,
  withStyles, Grid, SwipeableDrawer
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { Link } from 'react-router-dom';
import myClasses from './ResponsiveAppBar.module.css'

const styleSheet = {
  list : {
    width : 200,
  },
  padding : {
    paddingRight : 30,
    cursor : "pointer",
  },

  sideBarIcon : {
    padding : 0,
    color : "white",
    cursor : "pointer",
  }
}
class ResponsiveAppBar extends Component{

  constructor(props){
    super(props);
    this.state = {drawerActivate:false, drawer:false};
    this.createDrawer = this.createDrawer.bind(this);
    this.destroyDrawer = this.destroyDrawer.bind(this);
  }

  componentDidMount(){
    if(window.innerWidth < 768){
      this.setState({drawerActivate:true});
    }

    window.addEventListener('resize',()=>{
      if(window.innerWidth < 768){
        this.setState({drawerActivate:true});
      }
      else{
        this.setState({drawerActivate:false})
      }
    });
  }

  //Small Screens
  createDrawer(){
    return (
      <div>
        <AppBar {...AppBarStyle}>
          <Toolbar>
            <Grid container direction = "row" justify = "space-between" alignItems="center">
              <MenuIcon
                className = {this.props.classes.sideBarIcon}
                onClick={()=>{this.setState({drawer:true})}} />

              <Typography color="inherit" variant = "headline">{this.props.title}</Typography>
              <Typography color="inherit" variant = "headline"></Typography>
            </Grid>
          </Toolbar>
        </AppBar>

        <SwipeableDrawer
         open={this.state.drawer}
         onClose={()=>{this.setState({drawer:false})}}
         onOpen={()=>{this.setState({drawer:true})}}>

           <div
             tabIndex={0}
             role="button"
             onClick={()=>{this.setState({drawer:false})}}
             onKeyDown={()=>{this.setState({drawer:false})}}>

<List className = {this.props.classes.list}>
            
            {
              this.props.menuItems.map((item, index) => {
              return  <Link to={item.link}><ListItem key = {index} button divider>{item.title}</ListItem></Link>
            })}
             </List>

         </div>
       </SwipeableDrawer>

      </div>
    );
  }

  //Larger Screens
  destroyDrawer(){
    const {classes} = this.props
    return (
      <AppBar {...AppBarStyle}>
        <Toolbar>
          <Typography variant = "headline" style={{flexGrow:1}} color="inherit" >{this.props.title}</Typography>
          {this.props.menuItems.map((item, index) => {
            return <Link onClick={(index) => console.log(index)} to={item.link}><Typography key={index} variant = "subheading" className = {[classes.padding, myClasses.menuItems].join(' ')} color="inherit" >{item.title}</Typography></Link>
            })}
        </Toolbar>
      </AppBar>
    )
  }

  render(){
    return(
      <div>
        {this.state.drawerActivate ? this.createDrawer() : this.destroyDrawer()}
      </div>
    );
  }
}

ResponsiveAppBar.propTypes = {
  classes : PropTypes.object.isRequired,
  title : PropTypes.object.isRequired,
  menuItems : PropTypes.object.isRequired
};

const AppBarStyle = {
  position: 'sticky',
  style: {backgroundColor: "var(--primaryColor)"}
}

export default withStyles(styleSheet)(ResponsiveAppBar);
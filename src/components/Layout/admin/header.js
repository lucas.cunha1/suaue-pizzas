import React from 'react'
import { Nav, NavDropdown } from 'react-bootstrap'
import { useSelector } from 'react-redux'
import { getUser, removeToken } from '../../../config/auth'
import history from '../../../config/history.js'

const Header = () => {
    const profile = useSelector(state => state.auth.profile)
    console.log('profile', profile)
    const logout = () => {
        removeToken()
        history.push('/')
    }

    return (
        <Nav className="mr-auto navbar navbar-expand bg-white mb-4">
            <NavDropdown title={profile.name} id="dropdown-basic" className="ml-auto">
                <NavDropdown.Item>Perfil</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item onClick={logout}>Sair</NavDropdown.Item>
            </NavDropdown>
        </Nav>
    )
}
export default Header
